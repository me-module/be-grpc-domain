// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v4.25.1
// source: warehouse/warehouse.proto

package warehouse

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GetWarehouseRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id []string `protobuf:"bytes,1,rep,name=id,proto3" json:"id,omitempty"`
}

func (x *GetWarehouseRequest) Reset() {
	*x = GetWarehouseRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_warehouse_warehouse_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetWarehouseRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetWarehouseRequest) ProtoMessage() {}

func (x *GetWarehouseRequest) ProtoReflect() protoreflect.Message {
	mi := &file_warehouse_warehouse_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetWarehouseRequest.ProtoReflect.Descriptor instead.
func (*GetWarehouseRequest) Descriptor() ([]byte, []int) {
	return file_warehouse_warehouse_proto_rawDescGZIP(), []int{0}
}

func (x *GetWarehouseRequest) GetId() []string {
	if x != nil {
		return x.Id
	}
	return nil
}

type GetWarehouseByProviderRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ProviderId string `protobuf:"bytes,1,opt,name=provider_id,json=providerId,proto3" json:"provider_id,omitempty"`
}

func (x *GetWarehouseByProviderRequest) Reset() {
	*x = GetWarehouseByProviderRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_warehouse_warehouse_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetWarehouseByProviderRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetWarehouseByProviderRequest) ProtoMessage() {}

func (x *GetWarehouseByProviderRequest) ProtoReflect() protoreflect.Message {
	mi := &file_warehouse_warehouse_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetWarehouseByProviderRequest.ProtoReflect.Descriptor instead.
func (*GetWarehouseByProviderRequest) Descriptor() ([]byte, []int) {
	return file_warehouse_warehouse_proto_rawDescGZIP(), []int{1}
}

func (x *GetWarehouseByProviderRequest) GetProviderId() string {
	if x != nil {
		return x.ProviderId
	}
	return ""
}

type WarehouseData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             string                       `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name           string                       `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	ProviderId     string                       `protobuf:"bytes,3,opt,name=provider_id,json=providerId,proto3" json:"provider_id,omitempty"`
	Address        string                       `protobuf:"bytes,4,opt,name=address,proto3" json:"address,omitempty"`
	Province       string                       `protobuf:"bytes,5,opt,name=province,proto3" json:"province,omitempty"`
	District       string                       `protobuf:"bytes,6,opt,name=district,proto3" json:"district,omitempty"`
	PostalCode     string                       `protobuf:"bytes,7,opt,name=postal_code,json=postalCode,proto3" json:"postal_code,omitempty"`
	City           string                       `protobuf:"bytes,8,opt,name=city,proto3" json:"city,omitempty"`
	CreatedAt      string                       `protobuf:"bytes,9,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt      string                       `protobuf:"bytes,10,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt      string                       `protobuf:"bytes,11,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
	Availabilities []*WarehouseAvailabilityData `protobuf:"bytes,12,rep,name=availabilities,proto3" json:"availabilities,omitempty"`
}

func (x *WarehouseData) Reset() {
	*x = WarehouseData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_warehouse_warehouse_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *WarehouseData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*WarehouseData) ProtoMessage() {}

func (x *WarehouseData) ProtoReflect() protoreflect.Message {
	mi := &file_warehouse_warehouse_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use WarehouseData.ProtoReflect.Descriptor instead.
func (*WarehouseData) Descriptor() ([]byte, []int) {
	return file_warehouse_warehouse_proto_rawDescGZIP(), []int{2}
}

func (x *WarehouseData) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *WarehouseData) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *WarehouseData) GetProviderId() string {
	if x != nil {
		return x.ProviderId
	}
	return ""
}

func (x *WarehouseData) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *WarehouseData) GetProvince() string {
	if x != nil {
		return x.Province
	}
	return ""
}

func (x *WarehouseData) GetDistrict() string {
	if x != nil {
		return x.District
	}
	return ""
}

func (x *WarehouseData) GetPostalCode() string {
	if x != nil {
		return x.PostalCode
	}
	return ""
}

func (x *WarehouseData) GetCity() string {
	if x != nil {
		return x.City
	}
	return ""
}

func (x *WarehouseData) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *WarehouseData) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *WarehouseData) GetDeletedAt() string {
	if x != nil {
		return x.DeletedAt
	}
	return ""
}

func (x *WarehouseData) GetAvailabilities() []*WarehouseAvailabilityData {
	if x != nil {
		return x.Availabilities
	}
	return nil
}

type WarehouseAvailabilityData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ProviderId  string `protobuf:"bytes,2,opt,name=provider_id,json=providerId,proto3" json:"provider_id,omitempty"`
	WarehouseId string `protobuf:"bytes,3,opt,name=warehouse_id,json=warehouseId,proto3" json:"warehouse_id,omitempty"`
	Province    string `protobuf:"bytes,4,opt,name=province,proto3" json:"province,omitempty"`
	City        string `protobuf:"bytes,5,opt,name=city,proto3" json:"city,omitempty"`
	CreatedAt   string `protobuf:"bytes,6,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt   string `protobuf:"bytes,7,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *WarehouseAvailabilityData) Reset() {
	*x = WarehouseAvailabilityData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_warehouse_warehouse_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *WarehouseAvailabilityData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*WarehouseAvailabilityData) ProtoMessage() {}

func (x *WarehouseAvailabilityData) ProtoReflect() protoreflect.Message {
	mi := &file_warehouse_warehouse_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use WarehouseAvailabilityData.ProtoReflect.Descriptor instead.
func (*WarehouseAvailabilityData) Descriptor() ([]byte, []int) {
	return file_warehouse_warehouse_proto_rawDescGZIP(), []int{3}
}

func (x *WarehouseAvailabilityData) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *WarehouseAvailabilityData) GetProviderId() string {
	if x != nil {
		return x.ProviderId
	}
	return ""
}

func (x *WarehouseAvailabilityData) GetWarehouseId() string {
	if x != nil {
		return x.WarehouseId
	}
	return ""
}

func (x *WarehouseAvailabilityData) GetProvince() string {
	if x != nil {
		return x.Province
	}
	return ""
}

func (x *WarehouseAvailabilityData) GetCity() string {
	if x != nil {
		return x.City
	}
	return ""
}

func (x *WarehouseAvailabilityData) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *WarehouseAvailabilityData) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type GetWarehouseResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*WarehouseData `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *GetWarehouseResponse) Reset() {
	*x = GetWarehouseResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_warehouse_warehouse_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetWarehouseResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetWarehouseResponse) ProtoMessage() {}

func (x *GetWarehouseResponse) ProtoReflect() protoreflect.Message {
	mi := &file_warehouse_warehouse_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetWarehouseResponse.ProtoReflect.Descriptor instead.
func (*GetWarehouseResponse) Descriptor() ([]byte, []int) {
	return file_warehouse_warehouse_proto_rawDescGZIP(), []int{4}
}

func (x *GetWarehouseResponse) GetData() []*WarehouseData {
	if x != nil {
		return x.Data
	}
	return nil
}

var File_warehouse_warehouse_proto protoreflect.FileDescriptor

var file_warehouse_warehouse_proto_rawDesc = []byte{
	0x0a, 0x19, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2f, 0x77, 0x61, 0x72, 0x65,
	0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x77, 0x61, 0x72,
	0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x22, 0x25, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x57, 0x61, 0x72,
	0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x40, 0x0a,
	0x1d, 0x47, 0x65, 0x74, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x42, 0x79, 0x50,
	0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1f,
	0x0a, 0x0b, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x22,
	0x86, 0x03, 0x0a, 0x0d, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x44, 0x61, 0x74,
	0x61, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65,
	0x72, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x72, 0x6f, 0x76,
	0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73,
	0x12, 0x1a, 0x0a, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x12, 0x1a, 0x0a, 0x08,
	0x64, 0x69, 0x73, 0x74, 0x72, 0x69, 0x63, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x64, 0x69, 0x73, 0x74, 0x72, 0x69, 0x63, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x6f, 0x73, 0x74,
	0x61, 0x6c, 0x5f, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70,
	0x6f, 0x73, 0x74, 0x61, 0x6c, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x69, 0x74,
	0x79, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x63, 0x69, 0x74, 0x79, 0x12, 0x1d, 0x0a,
	0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a,
	0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x64,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x4c, 0x0a, 0x0e, 0x61, 0x76,
	0x61, 0x69, 0x6c, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x18, 0x0c, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x24, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x57,
	0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x69,
	0x6c, 0x69, 0x74, 0x79, 0x44, 0x61, 0x74, 0x61, 0x52, 0x0e, 0x61, 0x76, 0x61, 0x69, 0x6c, 0x61,
	0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x22, 0xdd, 0x01, 0x0a, 0x19, 0x57, 0x61, 0x72,
	0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x41, 0x76, 0x61, 0x69, 0x6c, 0x61, 0x62, 0x69, 0x6c, 0x69,
	0x74, 0x79, 0x44, 0x61, 0x74, 0x61, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64,
	0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x72, 0x6f,
	0x76, 0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x77, 0x61, 0x72, 0x65, 0x68,
	0x6f, 0x75, 0x73, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x77,
	0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x72,
	0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x72,
	0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x69, 0x74, 0x79, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x63, 0x69, 0x74, 0x79, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x44, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x57,
	0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x2c, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x18,
	0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x57, 0x61, 0x72, 0x65, 0x68,
	0x6f, 0x75, 0x73, 0x65, 0x44, 0x61, 0x74, 0x61, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x32, 0xbb,
	0x01, 0x0a, 0x09, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x12, 0x4d, 0x0a, 0x08,
	0x46, 0x69, 0x6e, 0x64, 0x42, 0x79, 0x49, 0x64, 0x12, 0x1e, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68,
	0x6f, 0x75, 0x73, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1f, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68,
	0x6f, 0x75, 0x73, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73,
	0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x5f, 0x0a, 0x10, 0x46,
	0x69, 0x6e, 0x64, 0x42, 0x79, 0x50, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12,
	0x28, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x57,
	0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x42, 0x79, 0x50, 0x72, 0x6f, 0x76, 0x69, 0x64,
	0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1f, 0x2e, 0x77, 0x61, 0x72, 0x65,
	0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75,
	0x73, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x2f, 0x5a, 0x2d,
	0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d, 0x65, 0x2d, 0x6d, 0x6f,
	0x64, 0x75, 0x6c, 0x65, 0x2f, 0x62, 0x65, 0x2d, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x64, 0x6f, 0x6d,
	0x61, 0x69, 0x6e, 0x2f, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_warehouse_warehouse_proto_rawDescOnce sync.Once
	file_warehouse_warehouse_proto_rawDescData = file_warehouse_warehouse_proto_rawDesc
)

func file_warehouse_warehouse_proto_rawDescGZIP() []byte {
	file_warehouse_warehouse_proto_rawDescOnce.Do(func() {
		file_warehouse_warehouse_proto_rawDescData = protoimpl.X.CompressGZIP(file_warehouse_warehouse_proto_rawDescData)
	})
	return file_warehouse_warehouse_proto_rawDescData
}

var file_warehouse_warehouse_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_warehouse_warehouse_proto_goTypes = []interface{}{
	(*GetWarehouseRequest)(nil),           // 0: warehouse.GetWarehouseRequest
	(*GetWarehouseByProviderRequest)(nil), // 1: warehouse.GetWarehouseByProviderRequest
	(*WarehouseData)(nil),                 // 2: warehouse.WarehouseData
	(*WarehouseAvailabilityData)(nil),     // 3: warehouse.WarehouseAvailabilityData
	(*GetWarehouseResponse)(nil),          // 4: warehouse.GetWarehouseResponse
}
var file_warehouse_warehouse_proto_depIdxs = []int32{
	3, // 0: warehouse.WarehouseData.availabilities:type_name -> warehouse.WarehouseAvailabilityData
	2, // 1: warehouse.GetWarehouseResponse.data:type_name -> warehouse.WarehouseData
	0, // 2: warehouse.Warehouse.FindById:input_type -> warehouse.GetWarehouseRequest
	1, // 3: warehouse.Warehouse.FindByProviderId:input_type -> warehouse.GetWarehouseByProviderRequest
	4, // 4: warehouse.Warehouse.FindById:output_type -> warehouse.GetWarehouseResponse
	4, // 5: warehouse.Warehouse.FindByProviderId:output_type -> warehouse.GetWarehouseResponse
	4, // [4:6] is the sub-list for method output_type
	2, // [2:4] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_warehouse_warehouse_proto_init() }
func file_warehouse_warehouse_proto_init() {
	if File_warehouse_warehouse_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_warehouse_warehouse_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetWarehouseRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_warehouse_warehouse_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetWarehouseByProviderRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_warehouse_warehouse_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*WarehouseData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_warehouse_warehouse_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*WarehouseAvailabilityData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_warehouse_warehouse_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetWarehouseResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_warehouse_warehouse_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_warehouse_warehouse_proto_goTypes,
		DependencyIndexes: file_warehouse_warehouse_proto_depIdxs,
		MessageInfos:      file_warehouse_warehouse_proto_msgTypes,
	}.Build()
	File_warehouse_warehouse_proto = out.File
	file_warehouse_warehouse_proto_rawDesc = nil
	file_warehouse_warehouse_proto_goTypes = nil
	file_warehouse_warehouse_proto_depIdxs = nil
}
